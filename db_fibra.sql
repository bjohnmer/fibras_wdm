-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 26, 2012 at 10:55 PM
-- Server version: 5.5.24
-- PHP Version: 5.3.10-1ubuntu3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_fibra`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_conectores`
--

CREATE TABLE IF NOT EXISTS `tbl_conectores` (
  `id_conector` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_conector` varchar(50) NOT NULL,
  `ppi_conector` double NOT NULL,
  PRIMARY KEY (`id_conector`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_conectores`
--

INSERT INTO `tbl_conectores` (`id_conector`, `descripcion_conector`, `ppi_conector`) VALUES
(1, '3M HOT MELT SC', 0.2),
(2, '3M HOT MELT ST', 0.2),
(3, '3M HOT MELT FC', 0.2),
(4, '3M CRIMPLOK SC', 0.2),
(5, '3M CRIMPLOK ST', 0.2),
(6, 'OPTRONICS SC MM-SM', 0.1),
(7, 'OPTRONICS LC MM-SM', 0.1),
(8, 'OPTRONICS ST MM-SM', 0.1),
(9, 'OPTRONICS MU MM-SM', 0.1),
(10, 'OPTRONICS MTRJ MM', 0.1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_edfaWDM`
--

CREATE TABLE IF NOT EXISTS `tbl_edfaWDM` (
  `id_edfawdm` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_edfawdm` varchar(50) NOT NULL,
  `nf_edfawdm` int(11) NOT NULL,
  `gain_edfawdm` int(11) NOT NULL,
  PRIMARY KEY (`id_edfawdm`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_edfaWDM`
--

INSERT INTO `tbl_edfaWDM` (`id_edfawdm`, `descripcion_edfawdm`, `nf_edfawdm`, `gain_edfawdm`) VALUES
(1, 'TELNET EDFA', 6, 20),
(2, 'MRV EM316EA-BR2022  EDFA', 6, 20),
(3, 'KAMELIAN EDFA', 6, 20);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fibras`
--

CREATE TABLE IF NOT EXISTS `tbl_fibras` (
  `id_fibra` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_fibra` varchar(50) NOT NULL,
  `ventana_fibra` int(11) NOT NULL,
  `atenuacion_fibra` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id_fibra`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `tbl_fibras`
--

INSERT INTO `tbl_fibras` (`id_fibra`, `descripcion_fibra`, `ventana_fibra`, `atenuacion_fibra`) VALUES
(3, 'OPTRONICS EXT. ARMADO 50/125', 800, 2.70),
(4, 'OPTRONICS EXT. ARMADO 50/125', 1300, 0.60),
(5, 'OPTRONICS EXT. ARMADO 62.5/125', 800, 2.30),
(6, 'OPTRONICS EXT. ARMADO 62.5/125', 1300, 0.60),
(7, 'OPTRONICS EXT. ARMADO 9/125', 1310, 0.35),
(8, 'OPTRONICS EXT. ARMADO 9/125', 1500, 0.22),
(9, 'OPTRONICS EXT. DIELECTRICO 50/125', 800, 2.70),
(10, 'OPTRONICS EXT. DIELECTRICO 50/125', 1300, 0.60),
(11, 'OPTRONICS EXT. DIELECTRICO 62.5/125', 800, 2.50),
(12, 'OPTRONICS EXT. DIELECTRICO 62.5/125', 1300, 0.70),
(13, 'OPTRONICS EXT. DIELECTRICO 9/125', 1310, 0.35),
(14, 'OPTRONICS EXT. DIELECTRICO 9/125', 1500, 0.22),
(15, 'OPTRONICS EXT. DIELECTRICO 9/125', 850, 3.00),
(16, 'OPTRONICS EXT. DIELECTRICO 9/125', 1300, 0.70),
(17, 'OPTRAL MM 50/125', 850, 2.50),
(18, 'OPTRAL MM 50/125', 1300, 0.70),
(19, 'OPTRAL SM10 G657', 1310, 0.35),
(20, 'OPTRAL SM10 G657', 1550, 0.21),
(21, 'OPTRAL SM10 NZDS', 1310, 0.40),
(22, 'OPTRAL SM10 NZDS', 1550, 0.25),
(23, 'OPTRAL SMF G652', 1310, 0.35),
(24, 'OPTRAL SMF G652', 1550, 0.23),
(25, 'TELNET SMF G.652.B', 1310, 0.37),
(26, 'TELNET SMF G.652.B', 1550, 0.24),
(27, 'TELNET SMF NZDS G.655', 1550, 0.24),
(28, 'TELNET SMF G.652.D', 1310, 0.37),
(29, 'TELNET SMF G.652.D', 1550, 0.24);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lo`
--

CREATE TABLE IF NOT EXISTS `tbl_lo` (
  `id_lo` int(11) NOT NULL AUTO_INCREMENT,
  `valor_lo` int(11) NOT NULL,
  PRIMARY KEY (`id_lo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_receptores`
--

CREATE TABLE IF NOT EXISTS `tbl_receptores` (
  `id_receptor` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_receptor` varchar(50) NOT NULL,
  `sensibilidad_receptor` double NOT NULL,
  `overload_receptor` double NOT NULL,
  `lo_receptor` int(11) NOT NULL,
  PRIMARY KEY (`id_receptor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_receptores`
--

INSERT INTO `tbl_receptores` (`id_receptor`, `descripcion_receptor`, `sensibilidad_receptor`, `overload_receptor`, `lo_receptor`) VALUES
(1, 'OPT-RX54 SMATV', -12, 5, 1310),
(2, 'OPT-RX51 SMATV', -12, 5, 1550);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_receptoresWDM`
--

CREATE TABLE IF NOT EXISTS `tbl_receptoresWDM` (
  `id_receptorwdm` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_receptorwdm` varchar(50) NOT NULL,
  `ber_receptorwdm` double NOT NULL,
  `bandwidth_receptorwdm` decimal(10,2) NOT NULL,
  `sensibilidad_receptorwdm` int(11) NOT NULL,
  `overload_receptorwdm` int(11) NOT NULL,
  PRIMARY KEY (`id_receptorwdm`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_receptoresWDM`
--

INSERT INTO `tbl_receptoresWDM` (`id_receptorwdm`, `descripcion_receptorwdm`, `ber_receptorwdm`, `bandwidth_receptorwdm`, `sensibilidad_receptorwdm`, `overload_receptorwdm`) VALUES
(1, 'ERICSSON APD PGR20312', -10, 2.50, -34, -3),
(2, 'FUJITSU FRM5N141GW ', -12, 10.50, -27, -5),
(3, 'MULTIPLEX RX192DLW ', -12, 9.50, -17, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transmisores`
--

CREATE TABLE IF NOT EXISTS `tbl_transmisores` (
  `id_transmisor` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_transmisor` varchar(50) NOT NULL,
  `pmax_transmisor` int(11) NOT NULL,
  `pmin_transmisor` int(11) NOT NULL,
  `lo_transmisor` int(11) NOT NULL,
  PRIMARY KEY (`id_transmisor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_transmisores`
--

INSERT INTO `tbl_transmisores` (`id_transmisor`, `descripcion_transmisor`, `pmax_transmisor`, `pmin_transmisor`, `lo_transmisor`) VALUES
(1, '1310 PICOMAN ARCOLAB', 14, 6, 1310),
(2, 'BEST ARCOLAB', 14, 11, 850),
(4, 'OTXS-xx TRAX', 13, 8, 1310);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transmisoresWDM`
--

CREATE TABLE IF NOT EXISTS `tbl_transmisoresWDM` (
  `id_transmisorwdm` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_transmisorwdm` varchar(50) NOT NULL,
  `potencia_transmisorwdm` int(11) NOT NULL,
  PRIMARY KEY (`id_transmisorwdm`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_transmisoresWDM`
--

INSERT INTO `tbl_transmisoresWDM` (`id_transmisorwdm`, `descripcion_transmisorwdm`, `potencia_transmisorwdm`) VALUES
(1, 'ERICSSON PGT 601 06 ', 3),
(2, 'JDSU DWDM SFP Optical Transceiver ', 4);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
