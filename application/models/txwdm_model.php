<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Txwdm_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	public function getall()
	{
		$consulta = $this->db->order_by('descripcion_transmisorwdm')->get('tbl_transmisoresWDM');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function get_tx($id_transmisor)
	{
		$consulta = $this->db->where('id_transmisorwdm', $id_transmisor)->order_by('descripcion_transmisorwdm')->get('tbl_transmisoresWDM');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

}
