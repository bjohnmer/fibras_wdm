<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rxwdm_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}


	public function get_rx($id_receptor)
	{
		$consulta = $this->db->where('id_receptorwdm', $id_receptor)->order_by('descripcion_receptorwdm')->get('tbl_receptoresWDM');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function getall()
	{
		$consulta = $this->db->order_by('descripcion_receptorwdm')->get('tbl_receptoresWDM');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}


}
