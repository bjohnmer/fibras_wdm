<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tx_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	public function getall_txbylongOnda($long_onda)
	{
		$consulta = $this->db->where('lo_transmisor', $long_onda)->order_by('descripcion_transmisor')->get('tbl_transmisores');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function get_tx($id_transmisor)
	{
		$consulta = $this->db->where('id_transmisor', $id_transmisor)->order_by('descripcion_transmisor')->get('tbl_transmisores');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function getall_txBetween($from, $to)
	{
		$consulta = $this->db->where('pmax_transmisor >',$to)->where('pmin_transmisor <',$to)->where('pmin_transmisor <=', $from)->where('pmax_transmisor >', $from)->order_by('descripcion_transmisor')->get('tbl_transmisores');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

}
