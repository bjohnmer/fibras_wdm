<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rx_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	public function getall_rxbylongOnda($long_onda)
	{
		$consulta = $this->db->where('lo_receptor', $long_onda)->order_by('descripcion_receptor')->get('tbl_receptores');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function get_rx($id_receptor)
	{
		$consulta = $this->db->where('id_receptor', $id_receptor)->order_by('descripcion_receptor')->get('tbl_receptores');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

}
