<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edfa_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	public function getall()
	{
		$consulta = $this->db->order_by('descripcion_edfawdm')->get('tbl_edfaWDM');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function get_edfa($id_edfa)
	{
		$consulta = $this->db->where('id_edfawdm', $id_edfa)->order_by('descripcion_edfawdm')->get('tbl_edfaWDM');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

}
