<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fibras_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	public function getall()
	{
		$consulta = $this->db->order_by('dispersion_fibra')->get('tbl_fibras');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function getall_lo()
	{
		$consulta = $this->db->select('ventana_fibra')->distinct('ventana_fibra')->order_by('ventana_fibra')->get('tbl_fibras');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function getall_fobylongOnda($long_onda)
	{
		$consulta = $this->db->where('ventana_fibra', $long_onda)->order_by('descripcion_fibra')->get('tbl_fibras');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	public function get_fo($id_fibra)
	{
		$consulta = $this->db->where('id_fibra', $id_fibra)->order_by('descripcion_fibra')->get('tbl_fibras');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

	

}
