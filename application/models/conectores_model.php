<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Conectores_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	public function getall()
	{
		$consulta = $this->db->order_by("descripcion_conector")->get('tbl_conectores');
		if ($consulta->num_rows()) {
			$data = $consulta->result();
		} else {
			$data = FALSE;
		}
		$consulta->free_result();
		return $data;
	}

}
