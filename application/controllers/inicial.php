<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicial extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		$this->load->library('form_validation');

		$this->load->model("fibras_model","fibras");
		$this->load->model("conectores_model","conectores");
		$this->load->model("tx_model","tx");
		$this->load->model("rx_model","rx");
		$this->load->model("rxwdm_model","rxwdm");
		$this->load->model("txwdm_model","txwdm");
		$this->load->model("edfa_model","edfa");

		$this->load->database();
		$this->load->helper('url');
		
		$this->load->library('grocery_CRUD');	
	}

	public function index()
	{
		$this->load->view('inicial');
	}




/********************************************************/
/*						Cálculos						*/
/********************************************************/

	public function getall_bylong_onda()
	{
		$long_onda = $_POST['long_onda'];
		$data['records'] = $this->fibras->getall_fobylongOnda($long_onda);
		$data['tx'] = $this->tx->getall_txbylongOnda($long_onda);
		$data['rx'] = $this->rx->getall_rxbylongOnda($long_onda);
		echo json_encode($data);
	}

	public function get_fo()
	{
		$id_fibra = $_POST['nombre_fibra'];
		$data['records'] = $this->fibras->get_fo($id_fibra);
		echo json_encode($data);
	}

	public function get_rx()
	{
		$id_receptor = $_POST['nombre_receptor'];
		$data['records'] = $this->rx->get_rx($id_receptor);
		echo json_encode($data);
	}

	public function get_rxwdm()
	{
		$id_receptor = $_POST['rx'];
		$data['rx'] = $this->rxwdm->get_rx($id_receptor);
		echo json_encode($data);
	}

	public function get_tx()
	{
		$id_transmisor = $_POST['nombre_transmisor'];
		$data['records'] = $this->tx->get_tx($id_transmisor);
		echo json_encode($data);
	}

	public function get_txwdm()
	{
		$id_transmisor = $_POST['tx'];
		$data['tx'] = $this->txwdm->get_tx($id_transmisor);
		echo json_encode($data);
	}

	public function get_edfa()
	{
		$id_edfa = $_POST['edfa'];
		$data['edfa'] = $this->edfa->get_edfa($id_edfa);
		echo json_encode($data);
	}

	public function get_max_dist()
	{


		$penalizacion 	= $_POST["penalizacion"];

		$long_onda 		= $_POST["long_onda"];
		$long_carrete	= $_POST["long_carrete"];
		$dist_enlace 	= $_POST["dist_enlace"];
		$nombre_fibra 	= $_POST["nombre_fibra"];
		$aten_esp 		= $_POST["aten_esp"];
		$nombre_trans 	= $_POST["nombre_trans"];
		$potencia_max 	= $_POST["potencia_max"];
		$potencia_min 	= $_POST["potencia_min"];
		$nombre_recep 	= $_POST["nombre_recep"];
		$sensibilidad 	= $_POST["sensibilidad"];
		$overload 		= $_POST["overload"];

		$num_empalmes	= ($dist_enlace / $long_carrete) - 1;

		$cant_cable = $dist_enlace + (0.015 * $dist_enlace) + (0.003 * $num_empalmes) + (0.03 * $num_empalmes) + 0.2 + 0.014;


		$aten_min = $potencia_min - $overload;
		$aten_max = $potencia_max - $sensibilidad - $penalizacion;
		$long_min = $aten_min / $aten_esp;
		$long_max = $aten_max / $aten_esp;


		$data['records']['num_empalmes'] = ceil($num_empalmes);
		$data['records']['cant_cable'] = round($cant_cable, 2);
		$data['records']['long_min'] = round($long_min,2);
		$data['records']['long_max'] = round($long_max,2);

		echo json_encode($data);

	}

	public function get_rango_dr()
	{

		$margen_diseno = $_POST["margen_diseno"];

		$long_onda 		= $_POST["long_onda"];
		$long_carrete	= $_POST["long_carrete"];
		$dist_enlace 	= $_POST["dist_enlace"];
		$nombre_fibra 	= $_POST["nombre_fibra"];
		$aten_esp 		= $_POST["aten_esp"];
		$nombre_trans 	= $_POST["nombre_trans"];
		$potencia_max 	= $_POST["potencia_max"];
		$potencia_min 	= $_POST["potencia_min"];
		$nombre_recep 	= $_POST["nombre_recep"];
		$sensibilidad 	= $_POST["sensibilidad"];
		$overload 		= $_POST["overload"];
		$tipo_empalme 	= $_POST["tipo_empalme"];
		$conectores 	= $_POST["conectores"];
		$conector 		= $_POST["conector"];

		$num_empalmes	= ($dist_enlace / $long_carrete) - 1;

		$cant_cable = $dist_enlace + (0.015 * $dist_enlace) + (0.003 * $num_empalmes) + (0.03 * $num_empalmes) + 0.2 + 0.014;


		$aten_fibra = $aten_esp * $cant_cable;
		$pp_empalme = $num_empalmes * $tipo_empalme;
		$pp_conector = $conectores * $conector;
		$pt_enlace = $aten_fibra + $pp_empalme + $pp_conector + $margen_diseno;
		$pr_max = $potencia_max - $pt_enlace;
		$pr_min = $potencia_min - $pt_enlace;
		

		$data['records']['num_empalmes'] = ceil($num_empalmes);
		$data['records']['cant_cable'] = round($cant_cable, 2);
		$data['records']['pr_max'] = round($pr_max,2);
		$data['records']['pr_min'] = round($pr_min,2);

		echo json_encode($data);

	}


	public function get_max_pr()
	{

		$margen_diseno = $_POST["margen_diseno"];

		$long_onda 		= $_POST["long_onda"];
		$long_carrete	= $_POST["long_carrete"];
		$dist_enlace 	= $_POST["dist_enlace"];
		$nombre_fibra 	= $_POST["nombre_fibra"];
		$aten_esp 		= $_POST["aten_esp"];
		$nombre_recep 	= $_POST["nombre_recep"];
		$sensibilidad 	= $_POST["sensibilidad"];
		$overload 		= $_POST["overload"];
		$tipo_empalme 	= $_POST["tipo_empalme"];
		$conectores 	= $_POST["conectores"];
		$conector 		= $_POST["conector"];

		$num_empalmes	= ($dist_enlace / $long_carrete) - 1;

		$cant_cable = $dist_enlace + (0.015 * $dist_enlace) + (0.003 * $num_empalmes) + (0.03 * $num_empalmes) + 0.2 + 0.014;
		$aten_fibra = $aten_esp * $cant_cable;
		$pp_empalme = $num_empalmes * $tipo_empalme;
		$pp_conector = $conectores * $conector;
		$pt_enlace = $aten_fibra + $pp_empalme + $pp_conector + $margen_diseno;
		$pot_min = $sensibilidad + $pt_enlace;
		$pot_max = $overload + $pt_enlace;
		

		$data['records']['num_empalmes'] = ceil($num_empalmes);
		$data['records']['pot_max'] = round($pot_max,2);
		$data['records']['pot_min'] = round($pot_min,2);


		$data['records']['tx'] = $this->tx->getall_txBetween(round($pot_min,2), round($pot_max,2));

		echo json_encode($data);

	}

	public function get_wdm()
	{


		$longenlace 	= $_POST['longenlace'];
		$fibra 			= $_POST['fibra'];
		$long_carrete 	= $_POST['long_carrete'];
		// $rx 			= $_POST['rx'];
		$ber 			= $_POST['ber'];
		$bandwidth 		= $_POST['bandwidth'];
		// $sensibilidad 	= $_POST['sensibilidad'];
		// $overload 		= $_POST['overload'];
		// $tx 			= $_POST['tx'];
		$potencia 		= $_POST['potencia'];
		// $edfa 			= $_POST['edfa'];
		$nf 			= $_POST['nf'];
		$gain 			= $_POST['gain'];
		$tipo_empalme 	= $_POST['tipo_empalme'];
		$conector 		= $_POST['conector'];

		$ancho_filtro = 20;

		$berf = pow(10,$ber);
		$k = sqrt(-2*log($berf));
		$ql = $k - ( (2.307 + (0.2706*$k)) / (1 + ($k * (0.9923 + (0.0448*$k)))));

		$q = 20*log10($ql);

		$osnr = $q - (10*log10($bandwidth/$ancho_filtro));

		$num = 0.01;
		
		$d = (int)(58 + $potencia - $nf - $osnr);
		do
		{
			$n = ceil($num);
			$i = (int)(((1.1 * $longenlace * $fibra) / $num) + ((($longenlace/($num*$long_carrete)) - 1)*$tipo_empalme) + (2*($conector)) + (10*log10($num)) );
			$num += 0.01;

		}while ($d <> $i);

 
		$balance = $potencia - ((((1.1 * $longenlace * $fibra) / $n) + ((($longenlace/($n*$long_carrete)) - 1)*$tipo_empalme) + (2*($conector))) * $n) + (($n-1)*$gain);

		$data['records']['distancia'] = round($longenlace/($n));
		$data['records']['amplificadores'] = $n-1;
		$data['records']['balance'] = round($balance,2);
		$data['records']['q'] = round($q,2);
		$data['records']['osnr'] = round($osnr,2);
		$data['records']['ql'] = $ql;

		echo json_encode($data);

	}

	public function get_efecto_disp()
	{


		$dispersion = $_POST["dispersion"];
		$ancho 		= $_POST["ancho"];
		$longitud	= $_POST["longitud"];

		$bandwidth = explode('E',strval((0.441*pow(10,-9))/($dispersion*$ancho*$longitud)));
		// $bandwidth = (0.441/(($dispersion*$ancho*$longitud)*pow(10,-9)));

		$data['bandwidth'] = round($bandwidth[0],2);

		echo json_encode($data);

	}


/********************************************************/
/*						Vistas							*/
/********************************************************/
	public function max_dist()
	{
		$data['records'] = $this->fibras->getall_lo();
		$this->load->view('max_dist', $data);
	}

	public function rango_dr()
	{

		$data['records'] = $this->fibras->getall_lo();
		$data['conectores'] = $this->conectores->getall();
		$this->load->view('rango_dr', $data);
	}

	public function max_pr()
	{
		$data['records'] = $this->fibras->getall_lo();
		$data['conectores'] = $this->conectores->getall();
		$this->load->view('max_pr', $data);
	}

	public function sistema_wdm()
	{
		$data['rx'] = $this->rxwdm->getall();
		$data['tx'] = $this->txwdm->getall();
		$data['edfa'] = $this->edfa->getall();
		$data['conectores'] = $this->conectores->getall();
		$this->load->view('sistema_wdm', $data);
	}

	public function efecto_disp()
	{
		$data['records'] = $this->fibras->getall();
		$this->load->view('efecto_disp', $data);
	}
	
	public function nuevas_func()
	{
		
	}

	public function registros()
	{
		$this->load->view('registros');
	}
	


/********************************************************/
/*						CRUD							*/
/********************************************************/

	public function lo()
	{
		try{

			$crud = new grocery_CRUD();

			$crud->set_table('tbl_lo');
			$crud->set_subject('Longitud de Onda');
			$crud->display_as("valor_lo", "Longitud de onda");
			
			$crud->set_rules("valor_lo", "Longitud de onda", 'required|numeric');

			$output = $crud->render();

			$this->load->view('lo',$output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function fo()
	{
		try{
			
			$crud = new grocery_CRUD();

			$crud->set_table('tbl_fibras');
			$crud->set_subject('Fibra Óptica');
			$crud->display_as("descripcion_fibra", "Descripción");
			$crud->display_as("ventana_fibra", "Ventana (nm)");
			$crud->display_as("atenuacion_fibra", "Atenuación (dB)");
			$crud->display_as("dispersion_fibra", "Dispersión Cromática (ps/nm*km)");
			
			$crud->set_rules("descripcion_fibra", "Descripción", 'required');
			$crud->set_rules("ventana_fibra", "Ventana (nm)", 'required|numeric');
			$crud->set_rules("atenuacion_fibra", "Atenuación (dB)", 'required|numeric');
			$crud->set_rules("dispersion_fibra", "Dispersión Cromática (ps/nm*km)", 'required|numeric');

			$output = $crud->render();
			

			$this->load->view('fo',$output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function tx()
	{
		try{
			
			$crud = new grocery_CRUD();

			$crud->set_table('tbl_transmisores');
			$crud->set_subject('Transmisor');
			$crud->display_as("descripcion_transmisor", "Descripción");
			$crud->display_as("pmax_transmisor", "Potencia Máxima (dBm)");
			$crud->display_as("pmin_transmisor", "Potencia Mínima (dBm)");
			$crud->display_as("lo_transmisor", "Longitud de Onda (nm)");


			$crud->set_rules("descripcion_transmisor", "Descripción", 'required');
			$crud->set_rules("pmax_transmisor", "Potencia Máxima (dBm)", 'required|numeric');
			$crud->set_rules("pmin_transmisor", "Potencia Mínima (dBm)", 'required|numeric');
			$crud->set_rules("lo_transmisor", "Longitud de Onda (nm)", 'required|numeric');


			$output = $crud->render();
			

			$this->load->view('tx',$output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function rx()
	{
		try{
			
			$crud = new grocery_CRUD();

			$crud->set_table('tbl_receptores');
			$crud->set_subject('Receptor');
			$crud->display_as("descripcion_receptor", "Descripción");
			$crud->display_as("sensibilidad_receptor", "Sensibilidad (dB)");
			$crud->display_as("overload_receptor", "Overload (dB)");
			$crud->display_as("pmax_receptor", "Potencia Máxima (dBm)");
			$crud->display_as("pmin_receptor", "Potencia Mínima (dBm)");
			$crud->display_as("lo_receptor", "Longitud de Onda (nm)");


			$crud->set_rules("descripcion_receptor", "Descripción", 'required');
			$crud->set_rules("sensibilidad_receptor", "Sensibilidad (dB)", 'required|numeric');
			$crud->set_rules("overload_receptor", "Overload (dB)", 'required|numeric');
			$crud->set_rules("pmax_receptor", "Potencia Máxima (dBm)", 'required|numeric');
			$crud->set_rules("pmin_receptor", "Potencia Mínima (dBm)", 'required|numeric');
			$crud->display_as("lo_receptor", "Longitud de Onda (nm)", 'required|numeric');

			$output = $crud->render();
			

			$this->load->view('rx',$output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function cn()
	{
		try{
			
			$crud = new grocery_CRUD();

			$crud->set_table('tbl_conectores');
			$crud->set_subject('Conector');
			$crud->display_as("descripcion_conector", "Descripción");
			$crud->display_as("ppi_conector", "Pérdida por Inserción (dB)");

			$crud->set_rules("descripcion_conector", "Descripción", 'required');
			$crud->set_rules("ppi_conector", "Pérdida por Inserción (dB)", 'required|numeric');

			$output = $crud->render();
			

			$this->load->view('cn',$output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function edfa()
	{
		try{
			
			$crud = new grocery_CRUD();

			$crud->set_table('tbl_edfaWDM');
			$crud->set_subject('Amplificador (WDM)');
			$crud->display_as("descripcion_edfawdm", "Descripción");
			$crud->display_as("nf_edfawdm", "Figura de Ruido (NF) [dB]");
			$crud->display_as("gain_edfawdm", "Ganancia [dB]");

			$crud->display_as("descripcion_edfawdm", "Descripción", 'required');
			$crud->display_as("nf_edfawdm", "Figura de Ruido (NF) [dB]", 'required|numeric');
			$crud->display_as("gain_edfawdm", "Ganancia [dB]", 'required|numeric');

			$output = $crud->render();
			

			$this->load->view('edfa',$output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function rxwdm()
	{
		try{
			
			$crud = new grocery_CRUD();

			$crud->set_table('tbl_receptoresWDM');
			$crud->set_subject('Receptor (WDM)');
			$crud->display_as("descripcion_receptorwdm", "Descripción");
			$crud->display_as("ber_receptorwdm", "Potencia del Bit Error Rate");
			$crud->display_as("bandwidth_receptorwdm", "Ancho de Banda Óptico [GHz]");

			$crud->display_as("descripcion_receptorwdm", "Descripción", 'required');
			$crud->display_as("ber_receptorwdm", "Potencia del Bit Error Rate", 'required|numeric');
			$crud->display_as("bandwidth_receptorwdm", "Ancho de Banda Óptico [GHz]", 'required|numeric');

			$output = $crud->render();
			

			$this->load->view('rxwdm',$output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function txwdm()
	{
		try{
			
			$crud = new grocery_CRUD();

			$crud->set_table('tbl_transmisoresWDM');
			$crud->set_subject('Transmisor (WDM)');
			$crud->display_as("descripcion_transmisorwdm", "Descripción");
			$crud->display_as("potencia_transmisorwdm", "Potencia (dBm)");

			$crud->display_as("descripcion_transmisorwdm", "Descripción", 'required');
			$crud->display_as("potencia_transmisorwdm", "Potencia (dBm)", 'required|numeric');

			$output = $crud->render();
			

			$this->load->view('txwdm',$output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

}