<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">

	<title>	.:: ULA ::. </title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">



	<?php 
	foreach($css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
	<?php endforeach; ?>
	<?php foreach($js_files as $file): ?>
		<script src="<?php echo $file; ?>"></script>
	<?php endforeach; ?>

	<!-- script src="<?=base_url()?>js/jquery-1.8.1.min.js"></script -->

	<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/default.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/calculos.css">

	<style type="text/css">
		section.contenido
		{
			width: 78%;
		}
	</style>
</head>
<body>
	<header>
		<div id="logo">
			<img src="<?=base_url()?>img/logo-ula.png" alt="">
		</div>

		<div id="info">
			<p>	
				Facultad de Ingeniería<br>
				Escuela de Ingeniería Eléctrica<br>
				Dpto. de Electrónica y Comunicaciones
			</p>
		</div>

		<p id="tit">
	 		NUEVO PROGRAMA DE COMPUTACIÓN PARA LA PLANIFICACIÓN Y DIMENSIONAMIENTO<br> DE SISTEMAS DE COMUNICACIÓN POR FIBRA ÓPTICA
	 	</p>

	</header>

	<?php require_once("menuprin.php"); ?>