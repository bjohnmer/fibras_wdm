<?php require_once('header.php');?>
	
	<section id="titulo">
	 	<article>
	 		<div id="icon"><img src="<?=base_url()?>img/ico5.png" alt=""></div>
	 		<p id="sub">Criterio </p> <p> MÁXIMA DISTANCIA</p>
	 	</article>
	 </section>

	 <section class="contenido">
	 	<article>
	 		<form id="form_max_dist">
				<?php if (validation_errors()) echo "<div class='alert alert-error'>".validation_errors()."</div>"; ?>
	 			
	 			<p>
		 			<label for="long-onda">
		 				Longitud de Onda
		 			</label>
		 			<select name="long-onda" id="long-onda">
		 				<option>Seleccione una Longitud de Onda</option>
					<?php if($records) : ?>
                        <?php  foreach($records as $row) : ?>
		 				<option value="<?=$row->ventana_fibra?>"><?=$row->ventana_fibra?> nm</option>
		 				<?php endforeach; ?>
                    <?php endif; ?>
		 			</select>
	 			</p>

	 			<p>
	 				<label for="dist-enlace">
	 					Distancia del Enlace
	 				</label>
	 				<input type="text" name"dist-enlace" id="dist-enlace"> <small>[km]</small>
	 			</p>

	 			
	 			<p>
		 			<label for="nombre-fibra">
		 				Fibra Óptica
		 			</label>
		 			<select name="nombre-fibra" id="nombre-fibra">
		 				<option value="0">Fibra Personalizada</option>
		 			</select>
	 			</p>

	 			<p>
		 			<label for="aten-esp">
		 				Atenuación Específica
		 			</label>
		 			<input type="text" name"aten-esp" id="aten-esp"> <small>[dB/km]</small>
	 			</p>
	 			
	 			<p>
	 				<label for="long-carrete">
	 					Longitud del Carrete
	 				</label>
	 				<select name="long-carrete" id="long-carrete">
	 					<option>Seleccione una Longitud de Carrete</option>
	 					<option value="0.5">0.5 [Km]</option>
	 					<option value="1">1 [Km]</option>
	 					<option value="2">2 [Km]</option>
	 				</select>
	 			</p>

	 			<p>
		 			<label for="nombre-trans">
		 				Transmisor Óptico
		 			</label>
		 			<select name="nombre-trans" id="nombre-trans">
		 				<option>Seleccione un Transmisor</option>
		 			</select>
	 			</p>


	 			<p>
		 			<label for="potencia-max">
		 				Potencia Máxima
		 			</label>
		 			<input type="text" name"potencia-max" id="potencia-max" readonly> <small>[dBm]</small>
	 			</p>

	 			<p>
		 			<label for="potencia-min">
		 				Potencia Mínima
		 			</label>
		 			<input type="text" name"potencia-min" id="potencia-min" readonly> <small>[dBm]</small>
	 			</p>

	 			<p>
		 			<label for="nombre-recep">
		 				Nombre del Receptor
		 			</label>
		 			<select name="nombre-recep" id="nombre-recep">
		 				<option>Seleccione un Receptor</option>
		 			</select>
	 			</p>

	 			<p>
	 				<label for="sensibilidad">
	 					Sensibilidad
		 			</label>
		 			<input type="text" name"sensibilidad" id="sensibilidad" readonly> <small>[dBm]</small>
	 			</p>

	 			<p>
		 			<label for="overload">
		 				Overload
		 			</label>
		 			<input type="text" name"overload" id="overload" readonly> <small>[dBm]</small>
	 			</p>

	 			<p>
	 				<label for="penalizacion">
	 					Penalización
	 				</label>
	 				<input type="text" name"penalizacion" id="penalizacion"><small>[dB]</small>
	 				<!-- <select name="margen_diseno" id="margen_diseno">
	 					<option>Seleccione un Margen de Diseño</option>
	 					<option value="2">2 [dB]</option>
	 					<option value="3">3 [dB]</option>
	 					<option value="4">4 [dB]</option>
	 					<option value="5">5 [dB]</option>
	 					<option value="6">6 [dB]</option>
	 				</select> -->
	 			</p>
	 			
	 			<p>
	 				<button>Calcular</button>
	 			</p>
	 		</form>
	 	</article>
	 </section>
	 <aside class="resultados">
	 	<article>
	 		<h1>Resultados</h1>
	 		<br><br>
	 		<p>
		 		<table width="100%" border="0" cellpadding="10" cellspacing="0">
		 			<tr>
		 		
		 				<td width="65%"><strong>Cantidad de Empalmes</strong></td>
		 				<td width="35%">&nbsp;<span class="num-empalmes"></span></td>
		 			</tr>
	 			        <tr>
		 				<td><strong>Longitud de Cable necesario</strong></td>
		 				<td>&nbsp;<span class="cant-cable"></span> [km]</td>
		 			</tr>
		 			<tr>
		 				<td><strong>Longitud Mínima</strong></td>
		 				<td>&nbsp; <span class="long-min"></span> [km]</td>
		 			</tr>
		 			<tr>
		 				<td><strong>Longitud Máxima</strong></td>
		 				<td>&nbsp; <span class="long-max"></span> [km]</td>
		 			</tr>
		 			<tr>
		 				<td colspan="2"><div id="mensaje" class=""></div></td>
		 			</tr>

		 		</table>
	 		</p>
	 	</article>
	 </aside>
	 <script type="text/javascript">
	 	$(document).ready(function(){
	        $("#long-onda").change(function () {

	            $("#long-onda option:selected").each(function () {

	                long_onda=$(this).val();
	                $.post('<?=base_url()?>inicial/getall_bylong_onda', { long_onda: long_onda }, function(data)
	                {
	                    $("#nombre-fibra").html('<option value="0">Fibra Personalizada</option>');
	                    for (i=0; i<data.records.length; i++) {
	                        if ((data.records[i])) {
	                        	$('#nombre-fibra').append("<option value='"+data.records[i]['id_fibra']+"'>"+data.records[i]['descripcion_fibra']+"</option>");
	                        }
	                    }

	                    $("#nombre-trans").html('<option>Seleccione un Transmisor</option>');
	                    for (i=0; i<data.tx.length; i++) {
	                        if ((data.tx[i])) {
	                        	$('#nombre-trans').append("<option value='"+data.tx[i]['id_transmisor']+"'>"+data.tx[i]['descripcion_transmisor']+"</option>");
	                        }
	                    }

	                    $("#nombre-recep").html('<option>Seleccione un Receptor</option>');
	                    for (i=0; i<data.rx.length; i++) {
	                        if ((data.rx[i])) {
	                        	$('#nombre-recep').append("<option value='"+data.rx[i]['id_receptor']+"'>"+data.rx[i]['descripcion_receptor']+"</option>");
	                        }
	                    }

	                }, 'json');

	            });
	        });

	        $("#nombre-fibra").change(function () {

	            $("#nombre-fibra option:selected").each(function () {

	                nombre_fibra=$(this).val();
	                if (nombre_fibra == 0)
	                {
	                	$("#aten-esp").removeAttr('readonly');
	                } else
	                {
	                	$("#aten-esp").attr('readonly', true);

	                	$.post('<?=base_url()?>inicial/get_fo', { nombre_fibra: nombre_fibra }, function(data)
		                {
		                	$('#aten-esp').val(data.records[0]['atenuacion_fibra']);

		                }, 'json'); 
	                }       
	            });
	        });

	        $("#nombre-fibra").change(function () {

	            $("#nombre-fibra option:selected").each(function () {

	                nombre_fibra=$(this).val();
	                $.post('<?=base_url()?>inicial/get_fo', { nombre_fibra: nombre_fibra }, function(data)
	                {

	                	$('#aten-esp').val(data.records[0]['atenuacion_fibra']);

	                }, 'json');        
	            });
	        });

	        $("#nombre-trans").change(function () {

	            $("#nombre-trans option:selected").each(function () {

	                nombre_transmisor=$(this).val();
	                $.post('<?=base_url()?>inicial/get_tx', { nombre_transmisor: nombre_transmisor }, function(data)
	                {
	                	$('#potencia-max').val(data.records[0]['pmax_transmisor']);
	                	$('#potencia-min').val(data.records[0]['pmin_transmisor']);

	                }, 'json');        
	            });
	        });	

	        $("#nombre-recep").change(function () {

	            $("#nombre-recep option:selected").each(function () {

	                nombre_receptor=$(this).val();
	                $.post('<?=base_url()?>inicial/get_rx', { nombre_receptor: nombre_receptor }, function(data)
	                {
	                	$('#sensibilidad').val(data.records[0]['sensibilidad_receptor']);
	                	$('#overload').val(data.records[0]['overload_receptor']);

	                }, 'json');        
	            });
	        });

	        $("#form_max_dist").submit(function () {
	        		

	        		long_onda		= $('#long-onda').val();
	        		long_carrete	= $('#long-carrete').val();
					dist_enlace		= $('#dist-enlace').val();
					nombre_fibra	= $('#nombre-fibra').val();
					aten_esp		= $('#aten-esp').val();
					nombre_trans	= $('#nombre-trans').val();
					potencia_max	= $('#potencia-max').val();
					potencia_min	= $('#potencia-min').val();
					nombre_recep	= $('#nombre-recep').val();
					sensibilidad	= $('#sensibilidad').val();
					overload		= $('#overload').val();
					penalizacion	= $('#penalizacion').val();

	                $.post('<?=base_url()?>inicial/get_max_dist', { 

	                	long_onda	: long_onda,
	                	long_carrete: long_carrete,
						dist_enlace	: dist_enlace,
						nombre_fibra: nombre_fibra,
						aten_esp	: aten_esp,
						nombre_trans: nombre_trans,
						potencia_max: potencia_max,
						potencia_min: potencia_min,
						nombre_recep: nombre_recep,
						sensibilidad: sensibilidad,
						overload	: overload,
						penalizacion: penalizacion

	                	}, 

	                	function(data)
		                {
		                	$('.num-empalmes').html(data.records.num_empalmes);
		                	$('.cant-cable').html(data.records.cant_cable);
		                	$('.long-min').html(data.records.long_min);
		                	$('.long-max').html(data.records.long_max);
		                	

		                	cc = parseFloat(data.records.cant_cable);
		                	lmi = parseFloat(data.records.long_min);
		                	lma = parseFloat(data.records.long_max);

		                	
		                	if (cc >= lmi && cc <= lma) 
		                	{
		                		$('#mensaje').addClass('success_c').html("LOS EQUIPOS PARA EL ENLACE SON CORRECTOS");
		                	}
		                	else
		                	{
		                		$('#mensaje').addClass('error_c').html("LOS EQUIPOS PARA EL ENLACE NO SON CORRECTOS");
		                	}

		            }, 'json');

		            return false;

	        });

        });
	 </script>
<?php require_once('footer.php');?>
