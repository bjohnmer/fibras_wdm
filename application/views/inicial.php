<?php require_once('header.php');?>

	<section class="opcion">
		<a href="<?=base_url()?>inicial/max_dist">
	 		<article>
	 			<div id="icon"><img src="<?=base_url()?>img/ico5.png" alt=""></div>
	 			<p id="sub">Criterio </p> <p> MÁXIMA DISTANCIA</p>
	 		</article>
		</a>
	 </section>
	 
	 <section class="opcion last">
		<a href="<?=base_url()?>inicial/rango_dr">
		 	<article>
		 		<div id="icon"><img src="<?=base_url()?>img/ico4.png" alt=""></div>
		 		<p id="sub">Criterio </p> <p> RANGO DINÁMICO DEL RECEPTOR</p>
		 	</article>
	 	</a>
	 </section>

	 <section class="opcion">
		<a href="<?=base_url()?>inicial/max_pr">
	 	<article>
	 		<div id="icon"><img src="<?=base_url()?>img/ico1.png" alt=""></div>
	 		<p id="sub">Criterio </p> <p> MÁXIMA POTENCIA DEL TRANSMISOR</p>
	 	</article>
	 	</a>
	 </section>

	 <section class="opcion last">
		<a href="<?=base_url()?>inicial/sistema_wdm">
	 	<article>
	 		<div id="icon"><img src="<?=base_url()?>img/ico2.png" alt=""></div>
	 		<p id="sub">Sistema </p> <p> WDM</p>
	 	</article>
	 	</a>
	 </section>

	 <section class="opcion">
		<a href="<?=base_url()?>inicial/efecto_disp">
	 	<article>
	 		<div id="icon"><img src="<?=base_url()?>img/ico2.png" alt=""></div>
	 		<p>Efecto de la Dispersión en Fibras Monomodo</p>
	 	</article>
	 	</a>
	 </section>

	 <section class="opcion last">
		<a href="">
	 	<article>
	 		<div id="icon"><img src="<?=base_url()?>img/ico2.png" alt=""></div>
	 		<p>Futuras Funciones</p>
	 	</article>
	 	</a>
	 </section>

<?php require_once('footer.php');?>