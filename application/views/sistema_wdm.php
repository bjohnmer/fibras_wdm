<?php require_once('header.php');?>

	<section id="titulo">
	 	<article>
	 		<div id="icon"><img src="<?=base_url()?>img/ico2.png" alt=""></div>
	 		<p id="sub">Sistema </p> <p> WDM </p>
	 	</article>
	 </section>

	 <section class="contenido">
	 	<article>
	 		<form id="form_sistema_wdm">

	 			<p>
	 				<label for="longenlace">
	 					Longitud del Enlace
	 				</label>
	 				<input type="text" name"longenlace" id="longenlace"> <small>[km]</small>
	 			</p>

	 			<p>
	 				<label for="fibra">
	 					Tipo de Fibra
	 				</label>
	 				<select name="fibra" id="fibra">
	 					<option>Seleccione un Tipo de Fibra</option>
	 					<option value="0.25">SMF Fibra SM Estándar (0.25 dB/km)</option>
	 					<option value="0.23">DSF Fibra SM Dispersión Desplazada (0.23 dB/km)</option>
	 					<option value="0.22">NZDS Fibra SM Dispersión Desplazada No Nula (0.22 dB/km)</option>
	 				</select>
	 			</p>

	 			<p>
	 				<label for="long_carrete">
	 					Longitud del Carrete
	 				</label>
	 				<select name="long_carrete" id="long_carrete">
	 					<option>Seleccione una Longitud de Carrete</option>
	 					<option value="0.5">0.5 [km]</option>
	 					<option value="1">1 [km]</option>
	 					<option value="2">2 [km]</option>
	 				</select>
	 			</p>

	 			<p>
		 			<label for="rx">
		 				Receptor WDM
		 			</label>
		 			<select name="rx" id="rx">
		 				<option>Seleccione un Receptor</option>
					<?php if($rx) : ?>
                        <?php  foreach($rx as $row) : ?>
		 				<option value="<?=$row->id_receptorwdm?>"><?=$row->descripcion_receptorwdm?></option>
		 				<?php endforeach; ?>
                    <?php endif; ?>
		 			</select>
	 			</p>

	 			<p>
	 				<label for="ber">
	 					Potencia del BER
	 				</label>
	 				<input type="text" name"ber" id="ber" readonly>
	 			</p>

	 			<p>
	 				<label for="bandwidth">
	 					Ancho de Banda
	 				</label>
	 				<input type="text" name"bandwidth" id="bandwidth" readonly> <small>[GHz]</small>
	 			</p>

	 			<p>
	 				<label for="sensibilidad">
	 					Sensibilidad
	 				</label>
	 				<input type="text" name"sensibilidad" id="sensibilidad" readonly> <small>[dBm]</small>
	 			</p>
	 			
	 			<p>
	 				<label for="overload">
	 					Overload
	 				</label>
	 				<input type="text" name"overload" id="overload" readonly> <small>[dBm]</small>
	 			</p>


	 			<p>
		 			<label for="tx">
		 				Transmisor WDM
		 			</label>
		 			<select name="tx" id="tx">
		 				<option>Seleccione un Transmisor</option>
					<?php if($tx) : ?>
                        <?php  foreach($tx as $row) : ?>
		 				<option value="<?=$row->id_transmisorwdm?>"><?=$row->descripcion_transmisorwdm?></option>
		 				<?php endforeach; ?>
                    <?php endif; ?>
		 			</select>
	 			</p>

	 			<p>
	 				<label for="potencia">
	 					Potencia
	 				</label>
	 				<input type="text" name"potencia" id="potencia" readonly> <small>[dBm]</small>
	 			</p>


	 			<p>
		 			<label for="edfa">
		 				Amplificador WDM
		 			</label>
		 			<select name="edfa" id="edfa">
		 				<option>Seleccione un Amplificador</option>
					<?php if($edfa) : ?>
                        <?php  foreach($edfa as $row) : ?>
		 				<option value="<?=$row->id_edfawdm?>"><?=$row->descripcion_edfawdm?></option>
		 				<?php endforeach; ?>
                    <?php endif; ?>
		 			</select>
	 			</p>

	 			<p>
	 				<label for="nf">
	 					Figura de Ruido
	 				</label>
	 				<input type="text" name"nf" id="nf" readonly> <small>[dB]</small>
	 			</p>

	 			<p>
	 				<label for="gain">
	 					Ganancia
	 				</label>
	 				<input type="text" name"gain" id="gain" readonly> <small>[dB]</small>
	 			</p>

	 			<p>
		 			<label for="tipo-empalme">
		 				Tipo de Empalme
		 			</label>
		 			<select name="tipo-empalme" id="tipo-empalme">
		 				<option>Seleccione un Tipo de Empalme</option>
		 				<option value="0.2">Mecánico (0.2 dB)</option>
		 				<option value="0.1">Fusión (0.1 dB)</option>
		 			</select>
	 			</p>

	 			<p>
		 			<label for="conector">
		 				Conectores
		 			</label>
		 			<select name="conector" id="conector">
		 				<option>Seleccione un Conector</option>
		 				<?php if($conectores) : ?>
	                        <?php  foreach($conectores as $row) : ?>
			 					<option value="<?=$row->ppi_conector?>"><?=$row->descripcion_conector?> (<?=$row->ppi_conector?> [dB])</option>
			 				<?php endforeach; ?>
                    	<?php endif; ?>

		 			</select>
	 			</p>

	 			<p>
	 				<button>Calcular</button>
	 			</p>

	 		</form>
	 	</article>
	 </section>
	 <aside class="resultados">
	 	<article>
	 		<h1>Resultados</h1>
	 		<br><br>
	 		<p>
		 		<table width="100%" border="0" cellpadding="10" cellspacing="0">
		 			<tr>
		 		
		 				<td width="65%"><strong>Distancia entre Amplificadores</strong></td>
		 				<td width="35%">&nbsp;<span class="distancia"></span> [km]</td>
		 			</tr>
		 			<tr>
		 				<td><strong>Cantidad de Amplificadores</strong></td>
		 				<td>&nbsp;<span class="amplificadores"></span></td>
		 			</tr>
		 			<tr>
		 				<td><strong>Balance de Potencia</strong></td>
		 				<td>&nbsp;<span class="balance"></span> [dBm]</td>
		 			</tr>
		 			<tr>
		 				<td><strong>Factor Q</strong></td>
		 				<td>&nbsp;<span class="q"></span> [dB]</td>
		 			</tr>
		 			<tr>
		 				<td><strong>OSNR</strong></td>
		 				<td>&nbsp;<span class="osnr"></span> [dB]</td>
		 			</tr>
		 			<tr>
		 				<td colspan="2"><div id="mensaje" class=""></div></td>
		 			</tr>
		 		</table>
	 		</p>
	 	</article>
	 </aside>
	 <script type="text/javascript">
	 	$(document).ready(function(){

	        $("#rx").change(function () {

	            $("#rx option:selected").each(function () {

	                rx=$(this).val();
	                $.post('<?=base_url()?>inicial/get_rxwdm', { rx: rx }, function(data)
	                {
	                	$('#ber').val(data.rx[0]['ber_receptorwdm']);
	                	$('#bandwidth').val(data.rx[0]['bandwidth_receptorwdm']);
	                	$('#sensibilidad').val(data.rx[0]['sensibilidad_receptorwdm']);
	                	$('#overload').val(data.rx[0]['overload_receptorwdm']);

	                }, 'json');        
	            });
	        });

	        $("#tx").change(function () {

	            $("#tx option:selected").each(function () {

	                tx=$(this).val();
	                $.post('<?=base_url()?>inicial/get_txwdm', { tx: tx }, function(data)
	                {
	                	$('#potencia').val(data.tx[0]['potencia_transmisorwdm']);

	                }, 'json');        
	            });
	        });

	        $("#edfa").change(function () {

	            $("#edfa option:selected").each(function () {

	                edfa=$(this).val();
	                $.post('<?=base_url()?>inicial/get_edfa', { edfa: edfa }, function(data)
	                {
	                	$('#nf').val(data.edfa[0]['nf_edfawdm']);
	                	$('#gain').val(data.edfa[0]['gain_edfawdm']);

	                }, 'json');        
	            });
	        });

	        $("#form_sistema_wdm").submit(function () {
	        		
	        		longenlace 		= $('#longenlace').val();
					fibra 			= $('#fibra').val();
					long_carrete 	= $('#long_carrete').val();
					rx 				= $('#rx').val();
					ber 			= $('#ber').val();
					bandwidth 		= $('#bandwidth').val();
					sensibilidad 	= $('#sensibilidad').val();
					overload 		= $('#overload').val();
					tx 				= $('#tx').val();
					potencia 		= $('#potencia').val();
					edfa 			= $('#edfa').val();
					nf 				= $('#nf').val();
					gain 			= $('#gain').val();
					tipo_empalme	= $('#tipo-empalme').val();
					conector 		= $('#conector').val();


	                $.post('<?=base_url()?>inicial/get_wdm', { 

	                	longenlace 		: longenlace,
						fibra 			: fibra,
						long_carrete 	: long_carrete,
						rx 				: rx,
						ber 			: ber,
						bandwidth 		: bandwidth,
						sensibilidad 	: sensibilidad,
						overload 		: overload,
						tx 				: tx,
						potencia 		: potencia,
						edfa 			: edfa,
						nf 				: nf,
						gain 			: gain,
						tipo_empalme 	: tipo_empalme,
						conector 		: conector

	                	}, 

	                	function(data)
		                {
							$('.distancia').html(data.records.distancia);
							$('.amplificadores').html(data.records.amplificadores);
							$('.balance').html(data.records.balance);
							$('.q').html(data.records.q);
							$('.osnr').html(data.records.osnr);
		                	

		                	sen = parseInt(sensibilidad);
		                	ove = parseInt(overload);

		                	bal = parseInt(data.records.balance);

		                	if (bal < sen)
		                	{
		                		$('#mensaje').removeClass("error_c").removeClass("success_c").addClass('error_c').html("EL SISTEMA NO FUNCIONA<br><h3>Solución</h3><p> <ul><li>Aumente la Ganancia de los Amplificadores</li><li>Aumente la potencia del Transmisor</li></ul></p>");
		                	}
		                	else
		                	{
		                		if (bal > ove)
			                	{
			                		$('#mensaje').removeClass("error_c").removeClass("success_c").addClass('error_c').html("EL SISTEMA NO FUNCIONA<br><h3>Solución</h3><p> <ul><li>Disminuya la Ganancia de los Amplificadores</li><li>Disminuya la potencia del Transmisor</li></ul></p>");
			                	}
			                	else
			                	{
			                		$('#mensaje').removeClass("error_c").removeClass("success_c").addClass('success_c').html("LOS EQUIPOS PARA EL ENLACE SON CORRECTOS");
			                	}
		                	}

		            }, 'json');

		            return false;

	        });

        });
	 </script>
<?php require_once('footer.php');?>
