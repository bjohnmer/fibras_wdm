<?php require_once('header.php');?>

	<section id="titulo">
	 	<article>
	 		<div id="icon"><img src="<?=base_url()?>img/ico2.png" alt=""></div>
	 		<p>Efecto de la Dispersión en Fibras Monomodo</p>
	 	</article>
	 </section>

	 <section class="contenido">
	 	<article>
	 		<form id="form_efecto_disp">

	 			<p>
		 			<label for="dispersion">
	 					Dispersión Cromática
	 				</label>
		 			<select name="dispersion" id="dispersion">
		 				<option>Seleccione una dispersión</option>
		 				<?php if($records) : ?>
	                        <?php  foreach($records as $row) : ?>
			 					<option value="<?=$row->dispersion_fibra?>"><?=$row->descripcion_fibra?> -> <?=$row->dispersion_fibra?> [ps/nm*km]</option>
			 				<?php endforeach; ?>
                    	<?php endif; ?>

		 			</select>
	 			</p>

	 			<p>
	 				<label for="ancho">
	 					Ancho Espectral<br> de la Fuente
	 				</label>
	 				<input type="text" name"ancho" id="ancho"> <small>[nm]</small>
	 			</p>

	 			<p>
	 				<label for="longitud">
	 					Longitud del Enlace
	 				</label>
	 				<input type="text" name"longitud" id="longitud"> <small>[km]</small>
	 			</p>

	 			<p>
	 				<button>Calcular</button>
	 			</p>

	 		</form>
	 	</article>
	 </section>
	 <aside class="resultados">
	 	<article>
	 		<h1>Resultados</h1>
	 		<br><br>
	 		<p>
		 		<table width="100%" border="0" cellpadding="10" cellspacing="0">
		 			<tr>
		 		
		 				<td width="65%"><strong>Ancho de Banda</strong></td>
		 				<td width="35%">&nbsp;<span class="bandwidth"></span> [GHz]</td>
		 			</tr>
		 		</table>
	 		</p>
	 	</article>
	 </aside>
	 <script type="text/javascript">
	 	$(document).ready(function(){

	 		$("#form_efecto_disp").submit(function () {
	        		
	        		dispersion	= $('#dispersion').val();
					ancho		= $('#ancho').val();
					longitud	= $('#longitud').val();

	               $.post('<?=base_url()?>inicial/get_efecto_disp', { 

	                	dispersion	: dispersion,
	                	ancho		: ancho,
	                	longitud	: longitud

	                	}, 

	                	function(data)
		                {
		                	$('.bandwidth').html(data.bandwidth);

		            }, 'json');
		            return false;

	        });

        });
	 </script>
<?php require_once('footer.php');?>
