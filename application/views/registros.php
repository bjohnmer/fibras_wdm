<?php require_once('header.php');?>

	<!-- <section>
		<a href="<?=base_url()?>inicial/lo">
	 		<article>
	 			<div id="icon"><img src="<?=base_url()?>img/ico5.png" alt=""></div>
	 			<p> LONGITUDES DE ONDA</p>
	 		</article>
		</a>
	 </section> -->

	 <section class="opcion">
		<a href="<?=base_url()?>inicial/fo">
	 		<article>
	 			<div id="icon"><img src="<?=base_url()?>img/ico5.png" alt=""></div>
	 			<p> FIBRAS</p>
	 		</article>
		</a>
	 </section>

	 <section class="opcion last">
		<a href="<?=base_url()?>inicial/tx">
	 		<article>
	 			<div id="icon"><img src="<?=base_url()?>img/ico5.png" alt=""></div>
	 			<p> TRANSMISORES</p>
	 		</article>
		</a>
	 </section>

	 <section class="opcion">
		<a href="<?=base_url()?>inicial/rx">
	 		<article>
	 			<div id="icon"><img src="<?=base_url()?>img/ico5.png" alt=""></div>
	 			<p> RECEPTORES</p>
	 		</article>
		</a>
	 </section>

	 <section class="opcion last">
		<a href="<?=base_url()?>inicial/cn">
	 		<article>
	 			<div id="icon"><img src="<?=base_url()?>img/ico5.png" alt=""></div>
	 			<p> CONECTORES</p>
	 		</article>
		</a>
	 </section>

	 <section class="opcion">
		<a href="<?=base_url()?>inicial/edfa">
	 		<article>
	 			<div id="icon"><img src="<?=base_url()?>img/ico5.png" alt=""></div>
	 			<p> AMPLIFICADORES (WDM)</p>
	 		</article>
		</a>
	 </section>

	 <section class="opcion last">
		<a href="<?=base_url()?>inicial/rxwdm">
	 		<article>
	 			<div id="icon"><img src="<?=base_url()?>img/ico5.png" alt=""></div>
	 			<p> RECEPTORES (WDM)</p>
	 		</article>
		</a>
	 </section>

	 <section class="opcion fullwidth">
		<a href="<?=base_url()?>inicial/txwdm">
	 		<article>
	 			<div id="icon"><img src="<?=base_url()?>img/ico5.png" alt=""></div>
	 			<p> TRANSMISORES (WDM)</p>
	 		</article>
		</a>
	 </section>
	 

<?php require_once('footer.php');?>